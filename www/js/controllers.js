angular.module('neosApp.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('CostCtrl', function($scope) {
  $scope.opts = {
    val1: false,
    val2: false,
    val3: false,
    val4: false,
    val5: false,
    val6: true,
    val7: false
  };
  
  $scope.computedTotal = function () {
    var opts = $scope.opts;
    var pages = (opts.val1 * 300);
    var total = (opts.val2 ? 5000 : 0) + (opts.val3 ? 350 : 0) + (opts.val4 ? 850 : 0) + (opts.val5 ? 1500 : 0) + (opts.val6 ? 1050 : 0) + (opts.val7 ? 3500 : 0);
     return pages + total;
  }
});
