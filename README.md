# README #

Documents the necessary steps to get the application up and running:

Install [NodeJS](https://nodejs.org/en/)

Then install Ionic Framework:


```
npm install -g cordova ionic
```


Clone this repo or start an Ionic project:


```

ionic start myApp sidemenu
```


```

cd myApp
```


Add platforms:


```

ionic platform add ios
```



```

ionic platform add android
```


Build:


```

ionic build ios
```



```

ionic build android
```


Emulate:


```

ionic emulate ios
```



```

ionic emulate android
```


Serve in a browser locally:


```

ionic serve
```

Update local library:


```

ionic lib update
```


Add Sass:


```

ionic setup sass
```


Remove platform to troubleshoot plugins:


```

ionic platform remove ios
```


Generate Cordova Splash screen plugin with PSD or AI files in resources folder:


```

ionic resources
```


Android SDK - API Level 19 (4.4.2 - 4.4.4)

```

android list avd
```


```

android create avd --name test19 --target android-19 --abi x86
```


```

android delete avd -n Name (replace Name with yours)
```


Emulate for specific iOS

```

ionic emulate --target="iPhone"
```


